﻿module Intersector

    type Direction = | L | R | U | D
    type Coordinate = {x:int; y:int} // Record Type
    type Line = Coordinate List 

    // Active Pattern function
    let (|Prefix|_|) (d: string) (s: string) =
        if s.StartsWith(d) then Some(s.Substring(d.Length))
        else None
        
    let getMove instruction =
        match instruction with
        | Prefix "L" rest -> {x = -(rest|>int); y=0}
        | Prefix "R" rest -> {x = +(rest|>int); y=0}
        | Prefix "U" rest -> {x=0; y = +(rest|>int)}
        | Prefix "D" rest -> {x=0; y = -(rest|>int)}
        | _ -> {x=0; y=0}
    
    // turns the instruction coordinates into a list of locations
    // each one starting where the previous one finished
    let mover (acc:Coordinate) current =
        let newX = acc.x + current.x
        let newY = acc.y + current.y
        {x=newX; y=newY},  {x=newX; y=newY}
       
    // turns the U88 list of instructions in to coordinates 
    let getLine (start:Coordinate) (lineInstructions)  =
        let moves = 
            lineInstructions 
            |> List.map getMove
            |> List.mapFold mover start |> fst
            |> List.append [start]        
        moves
        
    let plotLine1 (start:Coordinate) (stop:Coordinate)=
         let lowX = min start.x stop.x
         let highX = max start.x stop.x
         let xPlot = [for i in lowX .. highX -> {x = i; y = start.y}]
         
         let lowY = min start.y stop.y
         let highY = max start.y stop.y
         let yPlot = [for i in lowY .. highY -> {x = stop.x; y = i}]
         yPlot |> List.append xPlot, stop

    let plotLines (lineCoords:Line) =
        let start = lineCoords.[0]
        lineCoords
        |> List.mapFold plotLine1 start
        |> fst |> List.concat
        
    let getIntersection listA listB =
        Set.intersect (Set.ofList listA) (Set.ofList listB)
        |> Seq.toList
        
    let manhattan (coordinates)=
        coordinates
        |> List.map (fun coord -> abs coord.x + abs coord.y)
        
    let remove coord list =
        list |> List.filter (fun distance -> distance <> (coord.x - coord.y))

    let findNearestIntersection (start:Coordinate) line1Instructions line2Instructions =
        let line1 = getLine start line1Instructions
                    |> plotLines
        let line2 = getLine start line2Instructions
                    |> plotLines 
        let intersections = getIntersection line1 line2 
        let distance = manhattan intersections |> remove start
        let closest = List.min (distance)
        closest
        


        
    
    

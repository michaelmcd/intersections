# Intersections

Plots two line that can intersect at several points. Returns the intersection coordinates closest to the origin (using Manhattan distance).

Exercise taken from the [Advent of Code day 3](https://adventofcode.com/2019/day/3).

The program is written in F#. 

#### Running the code:
    let line1aInstructions = ["R75"; "D30"; "R83"; "U83"; "L12"; "D49"; "R71"; "U7"; "L72"]
    let line1bInstructions = ["U62"; "R66"; "U55"; "R34"; "D71"; "R55"; "D58"; "R83"]
    let start = {x=0; y=0}:Coordinate
    
    let nearestIntersection1 = findNearestIntersection start line1aInstructions line1bInstructions

    printfn "Result should be: 159 and is: %i" nearestIntersection1


#### The Algorithm:

Step 1
> Transform the line drawing instructions in to coordinates.
> 
> E.g. R75,D30 becomes {x = 75, y = 0} and {x = 0, y = -30}
> 
> The function: `getLine start line1Instructions`

Step 2
> Compute the start location of each instructions coordinates based upon the preceding instructions stop position.
> 
> E.g. [{x = 75, y = 0} {x = 0, y = -30}] becomes [{x = 75, y = 0} and {x = 75, y = -30}]
> 
> The function: `plotLines`  i.e. the result of the `getLine` function above is |> piped to this function.

Step 3
> The intermediate steps are plotted.
> 
> E.g. [{x = 0, y = 0} {x = 3, y = 0}] is expanded to  [{x = 0, y = 0}{x = 1, y = 0}{x = 2, y = 0}{x = 3, y = 0}]
> 
> The function: `getLine` does the work here (this contains the only looping expression in this solution).
> Each instruction results in an list of coordinate steps which are finally concatenated into a single list.

Step 4
> The intersections of the two lines are found using F#'s Set operations.
>
> The function: `getIntersection` does this.

Step 5 
> The Manhattan distance is then computed for the intersections
> 
> The function: `manhattan` takes care of this (note we don't use the starting position as per the instructions for the task (see the Advent of Code link above).

Step 6
> The smallest Manhattan distance is returned

> The function: `List.min (distance)`
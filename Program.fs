﻿open System
open Intersector

[<EntryPoint>]
let main argv =

    
    let line2aInstructions = ["R98"; "U47"; "R26"; "D63"; "R33"; "U87"; "L62"; "D20"; "R33"; "U53"; "R51"]
    let line2bInstructions = ["U98"; "R91"; "D20"; "R16"; "D67"; "R40"; "U7"; "R15"; "U6"; "R7"]
    let nearestIntersection2 = findNearestIntersection start line2aInstructions line2bInstructions
    
    printfn "Result should be: 135 and is: %i" nearestIntersection2
    0
    
